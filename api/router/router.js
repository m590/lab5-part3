const express = require('express');
const gameCtrl = require('../controller/games.controller');
const publisherCtrl = require('../controller/publisher.ctrl');
const reviewCtrl = require('../controller/review.ctrl');

const router = express.Router();

router.route("/games")
    .get(gameCtrl.gamesGetAll)
    .post(gameCtrl.gamesAddOne);

router.route("/games/:gameId")
    .get(gameCtrl.gamesGetOne)
    .put(gameCtrl.gamesFullUpdateOne)
    .patch(gameCtrl.gamesPartialUpdateOne)
    .delete(gameCtrl.gamesDeleteOne);

router.route("/games/:gameId/publisher")
    .get(publisherCtrl.publisherGetOne)
    .post(publisherCtrl.publisherAddOne)
    .put(publisherCtrl.publisherFullUpdateOne)
    .patch(publisherCtrl.publisherPartialUpdateOne)
    .delete(publisherCtrl.publisherDeleteOne);

router.route("/games/:gameId/reviews")
    .get(reviewCtrl.reviewsGetAll)
    .post(reviewCtrl.reviewsAddOne);

router.route("/games/:gameId/reviews/:reviewId")
    .put(reviewCtrl.reviewsFullUpdateOne)
    .patch(reviewCtrl.reviewsPartialUpdateOne)
    .delete(reviewCtrl.reviewsDeleteOne);


module.exports = router;