const mongoose = require('mongoose');
const Game = mongoose.model("Game");
const ObjectId = mongoose.Types.ObjectId;

module.exports.reviewsGetAll = function (req, res) {
    const gameId = req.params.gameId;
    Game.findById(gameId).select('reviews').exec(function (err, game) {
        const response = {
            status: 200,
            content: game.reviews
        };

        if (err) {
            console.log('Error get games', err);
            response.status = 500;
            response.content = {content: 'Internal error'};
        } else if (!game) {
            response.status = 400;
            response.content = {content: 'Game not found'};
        }
        res.status(response.status).json(response.content);
    });
}

const _addReview = function (req, res, game) {
    game.reviews.push({
        name: req.body.name,
        review: req.body.review,
        date: new Date()
    })

    game.save(function (err, savedGame) {
        const response = {
            status: 201,
            content: savedGame
        };

        if (err) {
            console.log('Error creating review', err);
            response.status = 500;
            response.message = 'Internal error';
        }
        res.status(response.status).json(response.content);
    });
}

module.exports.reviewsAddOne = function (req, res) {
    const gameId = req.params.gameId;
    Game.findById(gameId).exec(function (err, game) {
        console.log("game", game);
        const response = {
            status: 201,
            content: game
        };
        if (err) {
            response.status = 500;
            response.content = err;
        } else if (!game) {
            response.status = 404;
            response.content = 'Game not found';
        }
        if (response.status === 201) {
            _addReview(req, res, game);
        } else {
            res.status(response.status).json(response.content);
        }
    });
}

const _fullUpdateReview = function (req, res, game) {
    const reviewId = req.params.reviewId;
    for (let i = 0; i < game.reviews.length; i++) {
        if (game.reviews[i]._id == reviewId) {
            game.reviews[i].name = req.body.name;
            game.reviews[i].review = req.body.review;
            break;
        }
    }
    game.save(function (err, savedGame) {
        const response = {
            status: 201,
            content: savedGame
        };

        if (err) {
            console.log('Error full update review', err);
            response.status = 500;
            response.message = 'Internal error';
        }
        res.status(response.status).json(response.content);
    });
}

module.exports.reviewsFullUpdateOne = function (req, res) {
    const gameId = req.params.gameId;
    Game.findById(gameId).select('reviews').exec(function (err, game) {
        const response = {
            status: 201,
            content: game
        };
        if (err) {
            response.status = 500;
            response.content = err;
        } else if (!game) {
            response.status = 404;
            response.content = 'Game not found';
        }
        if (response.status === 201) {
            _fullUpdateReview(req, res, game);
        } else {
            res.status(response.status).json(response.content);
        }
    });
}

const _partialUpdateReview = function (req, res, game) {
    const reviewId = req.params.reviewId;
    for (let i = 0; i < game.reviews.length; i++) {
        if (game.reviews[i]._id == reviewId) {
            if (req.body.name) {
                game.reviews[i].name = req.body.name;
            }
            if (req.body.review) {
                game.reviews[i].review = req.body.review;
            }
            break;
        }
    }
    game.save(function (err, savedGame) {
        const response = {
            status: 201,
            content: savedGame
        };

        if (err) {
            console.log('Error partially updating review', err);
            response.status = 500;
            response.message = 'Internal error';
        }
        res.status(response.status).json(response.content);
    });
}

module.exports.reviewsPartialUpdateOne = function (req, res) {
    const gameId = req.params.gameId;
    Game.findById(gameId).select('reviews').exec(function (err, game) {
        const response = {
            status: 201,
            content: game
        };
        if (err) {
            response.status = 500;
            response.content = err;
        } else if (!game) {
            response.status = 404;
            response.content = 'Game not found';
        }
        if (response.status === 201) {
            _partialUpdateReview(req, res, game);
        } else {
            res.status(response.status).json(response.content);
        }
    });
}

const _deleteReview = function (req, res, game) {
    const reviewId = req.params.reviewId;
    for (let i = 0; i < game.reviews.length; i++) {
        if (game.reviews[i]._id == reviewId) {
            game.reviews[i] = {};
            break;
        }
    }
    game.save(function (err, savedGame) {
        const response = {
            status: 201,
            content: savedGame
        };

        if (err) {
            console.log('Error delete review', err);
            response.status = 500;
            response.message = 'Internal error';
        }
        res.status(response.status).json(response.content);
    });
}

module.exports.reviewsDeleteOne = function (req, res) {
    const gameId = req.params.gameId;
    Game.findById(gameId).select('reviews').exec(function (err, game) {
        const response = {
            status: 201,
            content: game
        };
        if (err) {
            response.status = 500;
            response.content = err;
        } else if (!game) {
            response.status = 404;
            response.content = 'Game not found';
        }
        if (response.status === 201) {
            _deleteReview(req, res, game);
        } else {
            res.status(response.status).json(response.content);
        }
    });
}