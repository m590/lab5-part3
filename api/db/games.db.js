const mongoose = require('mongoose');
require('./games-model');

const url = process.env.db_url + process.env.db_name_game;

mongoose.connect(url, {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true}, function () {
    console.log('game db connected');
});